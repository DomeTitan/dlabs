package fr.univlittoral.dlabs.security;

import javax.servlet.http.HttpServletRequest;

import fr.univlittoral.dlabs.DTO.LoginRequestDTO;
import fr.univlittoral.dlabs.DTO.UserDTO;
import fr.univlittoral.dlabs.Service.DealService;
import fr.univlittoral.dlabs.Service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

/**
 * @author MDE
 *
 */
@RestController
@RequestMapping(value = "/public/deal/login")
@Transactional
public class LoginBD {

	private static final Logger logger = LoggerFactory.getLogger(LoginBD.class);

	@Autowired
	private AuthenticationProvider authenticationManager;

	@Autowired
	private UserService userService;


	/**
	 * methode de connexion d'un utilisateur
	 *
	 * @param request donn?es necessaire a la connexion
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public UserDTO login(@RequestBody final LoginRequestDTO request, final HttpServletRequest req) {

		// Controle des params obligatoires
		if (StringUtils.isEmpty(request.getIdentifiant()) || StringUtils.isEmpty(request.getMotDePasse())) {
			throw new BadCredentialsException("User/pwd must not be emtpy");

		}
		final Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getIdentifiant(), request.getMotDePasse()));
		if (authentication == null) {
			throw new BadCredentialsException("User/pwd incorrect");
		}

		final DlabsSpringUser utilisateur = (DlabsSpringUser) authentication.getPrincipal();
		logger.debug("New user logged : " + utilisateur.getUsername());

		return userService.findBy(utilisateur.getUsername());
	}

}
