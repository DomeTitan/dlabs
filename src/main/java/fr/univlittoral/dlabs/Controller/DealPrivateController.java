package fr.univlittoral.dlabs.Controller;

import fr.univlittoral.dlabs.DTO.CreationDealDTO;
import fr.univlittoral.dlabs.DTO.TemperatureDTO;
import fr.univlittoral.dlabs.Service.DealService;
import fr.univlittoral.dlabs.Service.TemperatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping(value = "/deal")
public class DealPrivateController {

    @Autowired
    private DealService dealService;

    @Autowired
    private TemperatureService temperatureService;

    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody CreationDealDTO deal) {
        dealService.create(deal);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/up")
    public void up(@RequestBody TemperatureDTO temperatureDTO) {
        temperatureService.create(temperatureDTO, 1);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/down")
    public void down(@RequestBody TemperatureDTO temperatureDTO) {
        temperatureService.create(temperatureDTO, -1);
    }
}
