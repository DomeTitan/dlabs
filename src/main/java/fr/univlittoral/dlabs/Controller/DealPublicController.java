package fr.univlittoral.dlabs.Controller;

import fr.univlittoral.dlabs.DTO.*;
import fr.univlittoral.dlabs.Service.DealService;
import fr.univlittoral.dlabs.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@CrossOrigin
@RestController
@RequestMapping(value = "/public/deal")
public class DealPublicController {

    @Autowired
    private DealService dealService;
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<DealDTO> findAll() {
        return dealService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DetailDealDTO findBy(@PathVariable int id) {
        return dealService.findBy(id);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public UserDTO register(@RequestBody RegisterDTO registerDTO) {
        try {
            return userService.register(registerDTO);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
