package fr.univlittoral.dlabs.DTO;

public class TemperatureDTO {
    private int userId;
    private int dealId;

    public TemperatureDTO() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }
}
