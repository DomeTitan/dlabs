package fr.univlittoral.dlabs.DTO;

import java.util.Date;

public class DetailDealDTO {
    private String imgUrl;
    private int temperature;
    private String title;
    private String creator;
    private String shopName;
    private Date date;
    private String shopLink;
    private String codePromo;
    private float priceOld;
    private float priceNew;
    private float remise;
    private String description;

    public DetailDealDTO() {
    }

    public DetailDealDTO(String imgUrl, int temperature, String title, String creator, String shopName, Date date, String shopLink, String codePromo, float priceOld, float priceNew, float remise, String description) {
        this.imgUrl = imgUrl;
        this.temperature = temperature;
        this.title = title;
        this.creator = creator;
        this.shopName = shopName;
        this.date = date;
        this.shopLink = shopLink;
        this.codePromo = codePromo;
        this.priceOld = priceOld;
        this.priceNew = priceNew;
        this.remise = remise;
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public String getCodePromo() {
        return codePromo;
    }

    public void setCodePromo(String codePromo) {
        this.codePromo = codePromo;
    }

    public float getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(float priceOld) {
        this.priceOld = priceOld;
    }

    public float getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(float priceNew) {
        this.priceNew = priceNew;
    }

    public float getRemise() {
        return remise;
    }

    public void setRemise(float remise) {
        this.remise = remise;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}