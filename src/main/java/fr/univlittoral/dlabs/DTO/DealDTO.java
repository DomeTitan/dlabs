package fr.univlittoral.dlabs.DTO;

import java.util.Date;

public class DealDTO {
    private String imgUrl;
    private int temperature;
    private String title;
    private String creator;
    private String shopName;
    private Date date;
    private String shopLink;
    private int id;

    public DealDTO() {
    }

    public DealDTO(String imgUrl, int temperature, String title, String creator, String shopName, Date date, String shopLink, int id) {
        this.imgUrl = imgUrl;
        this.temperature = temperature;
        this.title = title;
        this.creator = creator;
        this.shopName = shopName;
        this.date = date;
        this.shopLink = shopLink;
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
