package fr.univlittoral.dlabs.Service;

import fr.univlittoral.dlabs.DO.UserDO;
import fr.univlittoral.dlabs.DTO.RegisterDTO;
import fr.univlittoral.dlabs.DTO.UserDTO;
import fr.univlittoral.dlabs.Repository.IUserRepository;
import fr.univlittoral.dlabs.security.PasswordBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserService {

    @Autowired
    private IUserRepository iUserRepository;

    @Autowired
    private PasswordBO passwordBO;

    public UserDTO findBy(String name) {
        UserDO userDO = iUserRepository.findAllByPseudo(name);

        UserDTO userDTO = new UserDTO();
        userDTO.setId(userDO.getId());
        userDTO.setPseudo(userDO.getPseudo());
        return userDTO;
    }

    public UserDTO register(RegisterDTO registerDTO) throws Exception {
        if(Objects.equals(registerDTO.getPassword(), registerDTO.getConfirmPassword())) {
            UserDO userDO = new UserDO();
            userDO.setPseudo(registerDTO.getPseudo());
            userDO.setFirstName(registerDTO.getPrenom());
            userDO.setLastName(registerDTO.getNom());
            userDO.setPassword(passwordBO.encode(registerDTO.getPassword()));
            iUserRepository.save(userDO);

            UserDTO userDTO = new UserDTO();
            userDTO.setId(userDO.getId());
            userDTO.setPseudo(userDO.getPseudo());
            return userDTO;
        }
        else {
            throw new Exception("Le mot de passe et sa confirmation sont différents.");
        }
    }
}
