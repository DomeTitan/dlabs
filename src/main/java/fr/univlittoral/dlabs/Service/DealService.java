package fr.univlittoral.dlabs.Service;

import fr.univlittoral.dlabs.DO.DealDO;
import fr.univlittoral.dlabs.DO.TemperatureDO;
import fr.univlittoral.dlabs.DTO.*;
import fr.univlittoral.dlabs.Repository.IDealRepository;
import fr.univlittoral.dlabs.Repository.IUserRepository;
import fr.univlittoral.dlabs.websocket.WebSocketConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class DealService {

    @Autowired
    private IDealRepository iDealRepository;

    @Autowired
    private IUserRepository iUserRepository;

    @Autowired
    private SimpMessagingTemplate template;

    public Collection<DealDTO> findAll() {
        Collection<DealDO> deals = iDealRepository.findAll();
        Collection<DealDTO> dealDTOS = new ArrayList<>();

        for (DealDO deal : deals) {
            dealDTOS.add(new DealDTO(
                    deal.getImgUrl(),
                    calculeTemperature(deal.getTemperatures()),
                    deal.getTitle(),
                    deal.getUser().getPseudo(),
                    deal.getShopName(),
                    deal.getDate(),
                    deal.getShopLink(),
                    deal.getId()
            ));
        }

        return dealDTOS;
    }

    public DetailDealDTO findBy(int id) {
        DealDO deal = iDealRepository.findById(id).get();

        return new DetailDealDTO(
                deal.getImgUrl(),
                calculeTemperature(deal.getTemperatures()),
                deal.getTitle(),
                deal.getUser().getPseudo(),
                deal.getShopName(),
                deal.getDate(),
                deal.getShopLink(),
                deal.getPromoCode(),
                deal.getPriceOld(),
                deal.getPriceNew(),
                (1 - (deal.getPriceNew()/deal.getPriceOld())) * 100,
                deal.getDescription()
        );
    }

    public void create(CreationDealDTO deal) {
        DealDO newDeal = new DealDO();
        newDeal.setTitle(deal.getTitle());
        newDeal.setShopName(deal.getShopName());
        newDeal.setShopLink(deal.getShopLink());
        newDeal.setPriceOld(deal.getPriceOld());
        newDeal.setPriceNew(deal.getPriceNew());
        newDeal.setPromoCode(deal.getPromoCode());
        newDeal.setDate(deal.getDate());
        newDeal.setImgUrl(deal.getImgUrl());
        newDeal.setDescription(deal.getDescription());
        newDeal.setUser(iUserRepository.findById(deal.getCreator()).get());
        iDealRepository.save(newDeal);

        DealDTO temp = new DealDTO(
                newDeal.getImgUrl(),
                0,
                newDeal.getTitle(),
                newDeal.getUser().getPseudo(),
                newDeal.getShopName(),
                newDeal.getDate(),
                newDeal.getShopLink(),
                newDeal.getId()
        );

        template.convertAndSend(WebSocketConfig.CONTEXT_MESSAGE + "/liste", temp);
    }

    private int calculeTemperature(Collection<TemperatureDO> temperatures) {
        int temperature = 0;

        for (TemperatureDO temperatureDO : temperatures) {
            temperature += temperatureDO.getValue();
        }

        return temperature;
    }
}
