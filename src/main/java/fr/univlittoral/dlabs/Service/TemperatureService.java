package fr.univlittoral.dlabs.Service;

import fr.univlittoral.dlabs.DO.TemperatureDO;
import fr.univlittoral.dlabs.DTO.TemperatureDTO;
import fr.univlittoral.dlabs.Repository.IDealRepository;
import fr.univlittoral.dlabs.Repository.ITemperatureRepository;
import fr.univlittoral.dlabs.Repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemperatureService {

    @Autowired
    private ITemperatureRepository iTemperatureRepository;

    @Autowired
    private IDealRepository iDealRepository;

    @Autowired
    private IUserRepository iUserRepository;

    public void create(TemperatureDTO temperatureDTO, int temperature) {
        TemperatureDO temperatureDO = new TemperatureDO();
        temperatureDO.setValue(temperature);
        temperatureDO.setDeal(iDealRepository.getById(temperatureDTO.getDealId()));
        temperatureDO.setUser(iUserRepository.getById(temperatureDTO.getUserId()));
        iTemperatureRepository.save(temperatureDO);
    }
}
