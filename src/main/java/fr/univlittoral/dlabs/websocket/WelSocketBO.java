package fr.univlittoral.dlabs.websocket;

import fr.univlittoral.dlabs.DTO.DealDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class WelSocketBO {

    @Autowired
    private SimpMessagingTemplate template;

    public void send(final DealDTO message) throws Exception {
        template.convertAndSend(WebSocketConfig.CONTEXT_MESSAGE + "/liste", message);
    }
}
