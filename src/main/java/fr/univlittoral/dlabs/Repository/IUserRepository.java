package fr.univlittoral.dlabs.Repository;

import fr.univlittoral.dlabs.DO.UserDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.MANDATORY)
public interface IUserRepository extends JpaRepository<UserDO, Integer> {

    UserDO findAllByPseudo(String pseudo);
}
