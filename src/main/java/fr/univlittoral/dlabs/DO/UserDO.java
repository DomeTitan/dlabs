package fr.univlittoral.dlabs.DO;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "tbl_user")
public class UserDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "PSEUDO")
    private String pseudo;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "PASSWORD")
    private String password;

    @OneToMany(mappedBy = "user")
    private Collection<DealDO> deals;

    @OneToMany(mappedBy = "user")
    private Collection<TemperatureDO> temperatures;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<DealDO> getDeals() {
        return deals;
    }

    public void setDeals(Collection<DealDO> deals) {
        this.deals = deals;
    }

    public Collection<TemperatureDO> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(Collection<TemperatureDO> temperatures) {
        this.temperatures = temperatures;
    }
}
