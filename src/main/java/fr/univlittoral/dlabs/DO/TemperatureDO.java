package fr.univlittoral.dlabs.DO;

import javax.persistence.*;

@Entity
@Table(name = "tbl_temperature")
public class TemperatureDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "VALUE")
    private int value;

    @ManyToOne
    @JoinColumn(name = "FK_USER")
    private UserDO user;

    @ManyToOne
    @JoinColumn(name = "FK_DEAL")
    private DealDO deal;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public UserDO getUser() {
        return user;
    }

    public void setUser(UserDO user) {
        this.user = user;
    }

    public DealDO getDeal() {
        return deal;
    }

    public void setDeal(DealDO deal) {
        this.deal = deal;
    }
}
