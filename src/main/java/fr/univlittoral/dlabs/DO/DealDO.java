package fr.univlittoral.dlabs.DO;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "tbl_deal")
public class DealDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "SHOP_NAME")
    private String shopName;

    @Column(name = "SHOP_LINK")
    private String shopLink;

    @Column(name = "PRICE_OLD")
    private float priceOld;

    @Column(name = "PRICE_NEW")
    private float priceNew;

    @Column(name = "PROMO_CODE")
    private String promoCode;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "IMG_URL")
    private String imgUrl;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "FK_CREATOR")
    private UserDO user;

    @OneToMany(mappedBy = "deal")
    private Collection<TemperatureDO> temperatures;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public float getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(float priceOld) {
        this.priceOld = priceOld;
    }

    public float getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(float priceNew) {
        this.priceNew = priceNew;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserDO getUser() {
        return user;
    }

    public void setUser(UserDO user) {
        this.user = user;
    }

    public Collection<TemperatureDO> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(Collection<TemperatureDO> temperatures) {
        this.temperatures = temperatures;
    }
}
