
var app = new Vue({
    el : "#app",
    created() {
        this.init();
    },
    data : {
        inscription : []
    },
    methods : {
        init : function(){
            this.inscription = {
                pseudo: null,
                nom: null,
                prenom: null,
                password: null,
                confirmPassword: null
            }
        },
        add : function() {
            axios.post("http://localhost:8080/public/deal/register", this.selectedDeal)
                .then(window.location.href = "/dlabs/static/index.html")
                .catch(error => {
                    console.error("Erreur lors de l'inscription !", error);
                });
        }
    }
})