
var app = new Vue({
    el : "#app",
    created() {
        this.fetchData();
        axios.interceptors.request.use(async function(config){
            if(!config.url.includes('public')) {
                config.headers['Authorization'] = 'Basic ' + localStorage.getItem('auth');
            }
            return config;
        }, function(error) {
            return Promise.reject(error);
        });
        this.connect();
    },
    data : {
        deals : [],
        selectedDeal : null,
        stompClient : null
    },
    methods : {
        fetchData() {
            axios.get("http://localhost:8080/public/deal").then(response => this.deals = response.data);
        },
        afficherDetail : function (id) {
            axios.get("http://localhost:8080/public/deal/" + id).then(response => this.selectedDeal = response.data);
        },
        isAuth() {
            if(localStorage.getItem("auth")) {
                return true;
            }
            return false;
        },
        getLogin() {
            return localStorage.getItem("userName")
        },
        logout() {
            localStorage.removeItem("auth");
            localStorage.removeItem("userName");
            localStorage.removeItem("userId");
            location.reload();
        },
        up(deal) {
           axios.post("http://localhost:8080/deal/up", {
               userId : parseInt(localStorage.getItem("userId")),
               dealId : deal.id
           })
               .then(response => {deal.temperature += 1;})
        },
        down(deal) {
            axios.post("http://localhost:8080/deal/down", {
                userId : parseInt(localStorage.getItem("userId")),
                dealId : deal.id
            })
                .then(response => {deal.temperature -= 1;})
        },
        connect() {
            var socket = new SockJS("http://localhost:8080/public/dlabs");
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function (frame) {
                stompClient.subscribe("/message/liste", function (greeting) {
                    console.log("toto");
                    console.log(greeting);
                });
            });
        }
    }
})