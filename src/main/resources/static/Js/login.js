
var app = new Vue({
    el : "#app",
    data : {
        identifiant : "",
        motDePasse : ""
    },
    methods : {
        connect : function() {
            axios.post("http://localhost:8080/public/deal/login", {
                identifiant : this.identifiant,
                motDePasse : this.motDePasse
            })
                .then((response) => {

                    localStorage.setItem(
                        "auth",
                        btoa(this.identifiant + ":" + this.motDePasse)
                    );
                    localStorage.setItem(
                        "userId",
                        response.data.id
                    );
                    localStorage.setItem(
                        "userName",
                        response.data.pseudo
                    );
                    window.location.href = "/dlabs/static/index.html";
                })
                .catch(error => {
                    console.error("Erreur lors de la connexion !", error);
                });
        }
    }
})