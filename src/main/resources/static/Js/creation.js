
var app = new Vue({
    el : "#app",
    created() {
        this.init();
        axios.interceptors.request.use(async function(config){
            if(!config.url.includes('public')) {
                config.headers['Authorization'] = 'Basic ' + localStorage.getItem('auth');
            }
            return config;
        }, function(error) {
            return Promise.reject(error);
        });
        this.connect();
    },
    data : {
        deal : []
    },
    methods : {
        init : function(){
            this.deal = {
                title: null,
                shopName: null,
                priceOld: null,
                priceNew: null,
                promoCode: null,
                imgUrl: null,
                description: null,
                creator : parseInt(localStorage.getItem("userId"))
            }
        },
        add : function() {
            axios.post("http://localhost:8080/deal", this.deal)
                .then(response => {window.location.href = "/dlabs/static/index.html"})
        },
        connect() {
            var socket = new SockJS("http://localhost:8080/public/dlabs");
            var stompClient = Stomp.over(socket);
            stompClient.connect({}, function (frame) {
                stompClient.subscribe("/message/liste", function (greeting) {
                    console.log("toto");
                    console.log(greeting);
                });
            });
        }
    }
})