package fr.univlittoral.dlabs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

class DlabsApplicationTests {

    @Test
    void contextLoads() {
        Assertions.assertEquals(1, 1);
    }

}
